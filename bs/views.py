from django.shortcuts import render

def home(request):
    return render(request, 'bs/home.html', {'title': 'Home'})
